#ifndef CONVERSATIONS_H
#define CONVERSATIONS_H

#include <conversation.h>
#include <contacts.h>
#include <QDateTime>

class Conversations
{
public:
    Conversations();

    bool addConversation(QString name, QVector <contact> contacts);
    void deleteConversation(int arrayID);
    void addConversationFromBase(conversation newConversation);
    conversation* getConversation(int arrayID);

    QVector < conversation > getConversations();
private:
    QVector < conversation > conversationList;
    QString generate_id();
};

#endif // CONVERSATIONS_H
