#include "newcontact.h"
#include "ui_newcontact.h"

NewContact::NewContact(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewContact)
{
    ui->setupUi(this);
}

NewContact::~NewContact()
{
    delete ui;
}

void NewContact::set_widget_pointer(Widget *np)
{
    p=np;
}

void NewContact::on_pushButton_clicked()
{
    QString text = ui->textEdit->toPlainText();
    QString a1 = ui->ip1->toPlainText();
    QString a2 = ui->ip2->toPlainText();
    QString a3 = ui->ip3->toPlainText();
    QString a4 = ui->ip4->toPlainText();
    if(!text.isEmpty() && !a1.isEmpty() && !a2.isEmpty() && !a3.isEmpty() && !a4.isEmpty()){
        text.remove("\n");
        newContact.nick = text;
        newContact.adresIP = QString(a1)+":"+QString(a2)+":"+QString(a3)+":"+QString(a4);
        p->add_contact(newContact);
        close();
    }
}

void NewContact::on_pushButton_2_clicked()
{
    close();
}

void NewContact::on_textEdit_textChanged()
{
    QString text = ui->textEdit->toPlainText();

    if(text.contains("\n")){
        if(text.count() >= 2) {
            text.remove("\n");
            newContact.nick = text;
            newContact.adresIP = "0.0.0.0:0000";
            p->add_contact(newContact);
            close();
        } else {
            text.clear();
            ui->textEdit->clear();
        }
    }//*/
}

void NewContact::on_ip1_textChanged()
{
    QString text = ui->ip1->toPlainText();
    QRegExp re("\\d*");
    if (!re.exactMatch(text) || text.count() > 3){
        ui->ip1->clear();
    }
}

void NewContact::on_ip2_textChanged()
{
    QString text = ui->ip2->toPlainText();
    QRegExp re("\\d*");
    if (!re.exactMatch(text) || text.count() > 3){
        ui->ip2->clear();
    }
}

void NewContact::on_ip3_textChanged()
{
    QString text = ui->ip3->toPlainText();
    QRegExp re("\\d*");
    if (!re.exactMatch(text) || text.count() > 3){
        ui->ip3->clear();
    }
}

void NewContact::on_ip4_textChanged()
{
    QString text = ui->ip4->toPlainText();
    QRegExp re("\\d*");
    if (!re.exactMatch(text) || text.count() > 3){
        ui->ip4->clear();
    }
}
