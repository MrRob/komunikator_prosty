#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDateTime>
#include <QMessageBox>
#include <QFileDialog>
#include <QXmlStreamWriter>
#include "emoji.h"
#include "contacts.h"
#include "conversations.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

signals:
    void popToLogin();

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    bool mockVision = true;
    bool isMock = true;
    QString nick;
    QString dateAndTimeFormat = "hh:mm:ss dd-MM-yyyy";
    void add_contact(contact newContact);
    void add_conversation(conversation newGroup);
    contact get_contact(int id);

//QString generate_id();

    void saveConvosXMLFile(QString filename);
    void saveContactsXMLFile(QString filename);
    void readConvosXMLFile(QString filename);
    void readContactsXMLFile(QString filename);
private slots:

    void setFootStamp(message message);

    void on_sendButton_clicked();

    void on_chatEdit_textChanged();

    void on_exitToolButton_clicked();

    void on_addContactToolButton_clicked();

    void on_deleteContactToolButton_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_mockToolButton_clicked();

    void on_addConversationToolButton_clicked();

    void on_conversationsListWidget_itemClicked();

    void on_deleteConversationToolButton_clicked();

private:
    QString contactFile = QFileDialog::getOpenFileName(this,
                                                    tr("Open File"), "../komunikator_prosty/listaKontaktow.xml",
                                                    tr("XML files (*.xml)"));
    QString conversationFile = QFileDialog::getOpenFileName(this,
                                                    tr("Open File"), "../komunikator_prosty/listaKonwersacji.xml",
                                                    tr("XML files (*.xml)"));
    contacts contactList;
    Conversations conversationList;
    conversation* currentConversation;
    Ui::Widget *ui;
};
#endif // WIDGET_H
