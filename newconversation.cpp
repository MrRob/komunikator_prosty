#include "newconversation.h"
#include "ui_newconversation.h"
#include <iostream>

NewConversation::NewConversation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewConversation)
{
    ui->setupUi(this);
}

NewConversation::~NewConversation()
{
    delete ui;
}

void NewConversation::set_widget_pointer(Widget *np)
{
    p=np;
}

void NewConversation::set_contact_view(QVector <contact> Contacts){
    for(int i=0; i<Contacts.size(); i++){
        ui->contacts->addItem(Contacts[i].nick);
        tmpContacts.append(Contacts);
        //ui->participants->addItem(Contacts[i].nick);
    }
}

void NewConversation::on_add_clicked()
{
    if(ui->contacts->selectedItems().size() != 0){
        int rowID = ui->contacts->currentRow();
        ui->participants->addItem(ui->contacts->item(rowID)->text());
        delete ui->contacts->takeItem(rowID);
        newContacts.append(tmpContacts[rowID]);
        tmpContacts.removeAt(rowID);
    }
}

/*void NewGroup::on_back_clicked()
{
    if(ui->participants->selectedItems().size() != 0){
        int rowID = ui->participants->currentRow();
        ui->contacts->addItem(ui->contacts->takeItem(rowID));
        delete ui->participants->takeItem(rowID);
        newContacts.removeAt(arrayID);
    }
}//*/

void NewConversation::on_save_clicked()
{
    QString newName = ui->name->toPlainText();
    if(newName.isEmpty() && newContacts.size()==1){
        ui->name->setPlainText(newContacts[0].nick);
        newName=newContacts[0].nick;
    }
    if(!newName.isEmpty()){
        newName.remove("\n");
        // newConversation.id = generate_id();
        newConversation.id = QDateTime::currentDateTime().toString("yyyyMMddhhmmss");
        newConversation.name = newName;
        newConversation.contacts=newContacts;
        p->add_conversation(newConversation);
        close();
    }
}

void NewConversation::on_deny_clicked()
{
    close();
}
