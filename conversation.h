#ifndef CONVERSATION_H
#define CONVERSATION_H

#include <QString>
#include <QVector>
#include "message.h"

struct conversation{
    QString id;
    QString name;
    QVector <contact> contacts;
    QVector <message> messages;
};

#endif // CONVERSATION_H
